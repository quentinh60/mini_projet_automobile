import csv
import os
import datetime

first_ordre_colonnes = ['address', 'carrosserie', 'categorie', 'couleur', 'cylindree', 'date_immat', 'denomination', 'energy', 'firstname', 'immat', 'marque', 'name', 'places', 'poids', 'puissance', 'type_variante_version', 'vin']
new_ordre_colonnes = {'adresse_titulaire': 'address', 'nom': 'name', 'prenom': 'firstname', 'immatriculation': 'immat', 'date_immatriculation': 'date_immat', 'vin': 'vin', 'marque': 'marque', 'denomination_commerciale': 'denomination', 'couleur': 'couleur','carrosserie': 'carrosserie', 'categorie': 'categorie', 'cylindree': 'cylindree', 'energie': 'energy', 'places': 'places', 'poids': 'poids', 'puissance': 'puissance', 'type': 'type_variante_version', 'variante': 'type_variante_version', 'version': 'type_variante_version'}

def verification_fichier(fichier_original):
    if(os.path.exists(fichier_original) is True):
        print("Le fichier : %s existe\n" %fichier_original)
    else:
        print("Le fichier : %s n'existe pas \n" %fichier_original)
    
    return(os.path.exists(fichier_original))

# Dans cette fonction on va simplement créer un nouveau fichier csv
#  à l'aide du fichier csv qu'on nous donne en le copiant intégralement.
def ecriture_newfichier(fichier,fichier_original):
    with open(fichier_original, 'r') as csvfile: 
        reader = csv.reader(csvfile, delimiter='|')
        with open(fichier, 'w', newline='') as newcsvfile:
            writer = csv.writer(newcsvfile, delimiter=';')
            for row in reader:
                writer.writerow(row)
    return newcsvfile

def changement_date(date):
    current_date = datetime.datetime.strptime(date,'%Y-%m-%d')
    date = current_date.strftime('%d/%m/%Y')
    return(date)

# Ici on va simplement modifier le fichier pour avoir la disposition qui nous a été demandé
#new_ordre correspond à la nouvelle disposition des colonnes
#first_ordre correspond à l'ancienne disposition des colonnes
#nom_colonne correspond à chaque valeur de first_ordre
#t_v_v correspond à un indice permettant de diviser la colonne type_variante_version
# en 3 colonnes : type, variante, version

def parcours_fichier(new_ordre, first_ordre, nom_colonne, row, t_v_v):
    newRow = ""
    saverow=[]
    if(new_ordre.get(nom_colonne) == "date_immat"):
        oldTime = row[first_ordre.index(new_ordre.get(nom_colonne))]
        newRow = changement_date(oldTime)
        return (newRow, t_v_v)
    if(new_ordre.get(nom_colonne) != "type_variante_version"):
        newRow = row[first_ordre.index(new_ordre.get(nom_colonne))]
        return (newRow, t_v_v)
    else:
        for data in row[first_ordre.index(new_ordre.get(nom_colonne))].split(','):
            saverow.append(data)
        t_v_v = t_v_v + 1
        return (saverow[t_v_v - 1], t_v_v)

#Cette fonctio va permettre de modifier le fichier selon la disposition recommandé
#On va donc faire une boucle for dans laquelle on va parcourir le fichier et ajouter 
# à chaque fois chaque ligne dans notre fichier csv pour à la fin retourner notre fichier csv.
def changement_colonnes(fichier, fichier_original, new_ordre_colonnes, first_ordre_colonnes):
    with open(fichier, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=';')
        nouvelle_disposition = []
        for nom_colonne in new_ordre_colonnes:
            nouvelle_disposition.append(nom_colonne)
        writer.writerow(nouvelle_disposition)
        index = 0
        with open(fichier_original, 'r') as ancienne_csvfile: 
            reader = csv.reader(ancienne_csvfile, delimiter='|')
            for row in reader:
                if index != 0:
                    nouvelle_ligne = []
                    t_v_v = 0
                    for nom_colonne in new_ordre_colonnes:
                        nouvelle_colonne, t_v_v = parcours_fichier(new_ordre_colonnes, first_ordre_colonnes ,nom_colonne, row, t_v_v)
                        if nouvelle_ligne != "":
                            nouvelle_ligne.append(nouvelle_colonne)
                    writer.writerow(nouvelle_ligne)
                index += 1
    return csvfile

def affichage_fichier(votrefichier, delimiteur):
    with open(votrefichier, 'r') as csvfile: 
        reader = csv.reader(csvfile, delimiter=delimiteur)
        for row in reader:
            print(row)
