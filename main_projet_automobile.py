from projet_automobile import *

#Main :

print("Entrez le nom du fichier à modifier : \n")
fichier_original = input()
fichier = "newfichier_automobile.csv"
verif = verification_fichier(fichier_original)

if (verif is True):
    ecriture_newfichier(fichier, fichier_original)
    changement_colonnes(fichier, fichier_original, new_ordre_colonnes, first_ordre_colonnes)
    affichage_fichier(fichier,';')