from projet_automobile import *
import unittest

class TestProjetAutomobile(unittest.TestCase):

    def test_verification_fichier(self):
        self.assertEqual(verification_fichier("automobile.csv"),True)
        self.assertEqual(verification_fichier("bidule.csv"),False)
        self.assertEqual(verification_fichier("automobile"),False)
    
    def test_ecriture_newfichier(self):
        newfichier="newfichier_automobile.csv"
        fichier_original="automobile.csv"
        ecriture_newfichier(newfichier,fichier_original)
        datarows = []
        with open(newfichier, 'r') as csvfile: 
            reader = csv.reader(csvfile, delimiter=';')
            for row in reader:
                datarows.append(row)
            self.assertEqual(['address', 'carrosserie', 'categorie', 'couleur', 'cylindree', 'date_immat', 'denomination', 'energy', 'firstname', 'immat', 'marque', 'name', 'places', 'poids', 'puissance', 'type_variante_version', 'vin'],datarows[0])
            self.assertEqual(['3822 Omar Square Suite 257 Port Emily, OK 43251', '45-1743376', '34-7904216', 'LightGoldenRodYellow', '3462', '2012-05-03', 'Enhanced well-modulated moderator', '37578077', 'Jerome', 'OVC-568', 'Williams Inc', 'Smith', '32', '3827', '110', 'Inc, 92-3625175, 79266482', '9780082351764'],datarows[1])
        
    def test_changement_colonnes(self):
        newfichier="newfichier_automobile.csv"
        fichier_original="automobile.csv"
        changement_colonnes(newfichier, fichier_original, new_ordre_colonnes, first_ordre_colonnes)
        datarows = []
        with open(newfichier, 'r') as csvfile: 
            reader = csv.reader(csvfile, delimiter=';')
            for row in reader:
                datarows.append(row)
            self.assertEqual(['adresse_titulaire', 'nom', 'prenom', 'immatriculation', 'date_immatriculation', 'vin', 'marque', 'denomination_commerciale', 'couleur', 'carrosserie', 'categorie', 'cylindree', 'energie', 'places', 'poids', 'puissance', 'type', 'variante', 'version'],datarows[0])
            self.assertEqual(['3822 Omar Square Suite 257 Port Emily, OK 43251', 'Smith', 'Jerome', 'OVC-568', '2012-05-03', '9780082351764', 'Williams Inc', 'Enhanced well-modulated moderator', 'LightGoldenRodYellow', '45-1743376', '34-7904216', '3462', '37578077', '32', '3827', '110', 'Inc', ' 92-3625175', ' 79266482'],datarows[1])

    
